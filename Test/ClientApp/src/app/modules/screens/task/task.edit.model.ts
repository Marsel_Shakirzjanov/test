import { Data } from "@angular/router";

export class EditTask {
  constructor(
    public id:number,
    public name: string,
    public description: string,
    public done: boolean,
    public deadline: Data

  ) {

  }
}
