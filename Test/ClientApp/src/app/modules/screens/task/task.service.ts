import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AddTask } from './task.model';
import { EditTask } from './task.edit.model';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';

@Injectable()
export class TaskService {

  constructor(private http: HttpClient) { }


  getTasks(): Observable<any> {
    return this.http.get<any>('/api/task/taskList');
  }
  createTask(editModel: AddTask) {
    return this.http.post('/api/task/create', editModel);
  }
  deleteTask(id: number) {
    return this.http.post('/api/task/delete', id);
  }
  taskEdit(taskModel: EditTask) {
    return this.http.post<any>('/api/task/edit', taskModel);
  }
  taskEditId(id: number): Observable<any> {
    return this.http.get<any>('/api/task/task/' + id);
  }
}
