import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TaskService } from '../task.service';
import { ChangeDetectorRef } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  taskList: any[];
  constructor(private changeDetectorRef: ChangeDetectorRef,private http: HttpClient, private router: Router, private taskService: TaskService) {

  }
  ngOnInit() {
    this.taskService.getTasks().subscribe(response => {
      console.log(response);
      this.taskList = response;
    });
  }
  deleteTask(id: number,taskId:number) {
    this.taskService.deleteTask(taskId).subscribe(_ => {
    });
    this.taskList.splice(id, 1);
    this.changeDetectorRef.detectChanges();
  }
  }
