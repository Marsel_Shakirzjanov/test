"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EditTask = /** @class */ (function () {
    function EditTask(id, name, description, done, deadline) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.done = done;
        this.deadline = deadline;
    }
    return EditTask;
}());
exports.EditTask = EditTask;
//# sourceMappingURL=task.edit.model.js.map