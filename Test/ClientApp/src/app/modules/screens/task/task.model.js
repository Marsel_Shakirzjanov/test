"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AddTask = /** @class */ (function () {
    function AddTask(name, description, done, deadline) {
        this.name = name;
        this.description = description;
        this.done = done;
        this.deadline = deadline;
    }
    return AddTask;
}());
exports.AddTask = AddTask;
//# sourceMappingURL=task.model.js.map