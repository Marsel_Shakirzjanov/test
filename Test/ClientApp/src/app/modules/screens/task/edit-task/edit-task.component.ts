import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../task.service';
import { EditTask } from '../task.edit.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  taskList: Observable<any>;
  loadedData: boolean = false;
  private id: number;
  public form: FormGroup;
  constructor(private http: HttpClient, private fb: FormBuilder, private route: ActivatedRoute, private taskService: TaskService, private router: Router)
  {
    this.id = this.route.snapshot.queryParams['id'];

    this.taskService.taskEditId(this.id)
      .subscribe(resp => {
        this.taskList = resp;
        this.loadedData = true;
      });


    this.form = this.fb.group({
      id: [this.id],
      name: ['', Validators.required],
      description: ['', Validators.required],
      done: ['', Validators.required],
      deadline: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  submit() {
    console.log(this.form.value);
    this.taskService.taskEdit(<EditTask>this.form.value).subscribe(_ => {
      this.router.navigateByUrl('');
    })
  }

}
