import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { validateConfig } from '@angular/router/src/config';
import { TaskService } from '../task.service';
import { AddTask } from '../task.model';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  public form: FormGroup;
  constructor(private http: HttpClient, private fb: FormBuilder, private taskService: TaskService, private router: Router)
  {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description:['', Validators.required],
      done:[false, Validators.required],
      deadline:['', Validators.required]

    });
  }

  ngOnInit() {
  }
  submit() {
    console.log(this.form.value);
    this.taskService.createTask(<AddTask>this.form.value).subscribe(_ => {
      this.router.navigateByUrl('');
    })
  }
  
}
