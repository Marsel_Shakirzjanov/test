export class AddTask {
  constructor(
    public name: string,
    public description: string,
    public done: boolean,
    public deadline:Date

  ) {

  }
}
