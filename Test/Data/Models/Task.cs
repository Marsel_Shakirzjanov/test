﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Data.BaseEntity;

namespace Test.Data.Models
{
    public class Task : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Done { get; set; }
        public DateTime Deadline { get; set; }
    }
}
