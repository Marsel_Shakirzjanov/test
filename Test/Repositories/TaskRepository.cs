﻿using System;
using System.Collections.Generic;
using System.Linq;
using Test.Data;
using Test.Data.Models;

namespace Test.Repositories
{
    public class TaskRepository:CRUDRepository<Task>
    {
        public TaskRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }

        
    }
}
