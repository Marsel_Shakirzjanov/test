﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Data;
using Test.Data.BaseEntity;

namespace Test.Repositories
{
    public class CRUDRepository<T> where T:BaseModel
    {
        protected readonly ApplicationDbContext _dbContext;
        public CRUDRepository(ApplicationDbContext context)
        {
            _dbContext = context;
        }
        public virtual IQueryable<T> GetAll()
        {
            return _dbContext.Set<T>().AsQueryable();
        }
        public virtual T Get(int id)
        {
            return _dbContext.Set<T>().FirstOrDefault(x => x.Id == id);
        }
        public void Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();
        }
        public void Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }
        public void Delete(int id)
        {
            _dbContext.Set<T>().Remove(_dbContext.Set<T>().FirstOrDefault(x => x.Id == id));
            _dbContext.SaveChanges();
        }
    }
}
