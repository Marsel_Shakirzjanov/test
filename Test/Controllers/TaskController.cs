﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Test.Data.Dto;
using Test.Repositories;
using Test.Data.Models;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        private readonly IMapper _mapper;
        private readonly TaskRepository _taskRepository;
        public TaskController(TaskRepository taskRepository, IMapper mapper)
        {
            this._mapper = mapper;
            _taskRepository = taskRepository;
        }
        [HttpPost("create")]
        public IActionResult Create([FromBody] TaskDto taskDto)
        {
            if (ModelState.IsValid)
            {
                var task = _mapper.Map<TaskDto, Data.Models.Task>(taskDto);
                _taskRepository.Add(task);
                return NoContent();
            }
            return BadRequest();
        }
        [HttpGet("taskList")]
        public IActionResult TaskList()
        {
            return Ok(_taskRepository.GetAll());
        }
        [HttpGet("task/{id}")]
        public IActionResult GetTask(int id)
        {
            return Ok(_taskRepository.Get(id));
        }
        [HttpPost("delete")]
        public IActionResult DeleteTask([FromBody]int id)
        {
            if (ModelState.IsValid)
            {
                _taskRepository.Delete(id);
                return NoContent();
            }
            return BadRequest();
        }
        [HttpPost("edit")]
        public IActionResult EditTask([FromBody] TaskDto taskDto)
        {
            if (ModelState.IsValid)
            {
                var task = _mapper.Map<TaskDto, Data.Models.Task>(taskDto);
                _taskRepository.Update(task);
                return NoContent();
            }
            return BadRequest();
        }
    }
}